import { INormalizedData } from '../../../seb-model/src/interface/INormalizedData';
import { IPartnershipRequest } from '../../../seb-model/src/interface/IPartnershipRequest';

export const SetPartnershipRequests = 'partnershipRequests/set-partnershipRequests';

export const setPartnershipRequests = (partnershipRequests : INormalizedData<IPartnershipRequest>) => {
    return {
        type : SetPartnershipRequests,
        payload : { partnershipRequests }
    };
};
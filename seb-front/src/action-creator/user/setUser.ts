import { INormalizedData } from '../../../../seb-model/src/interface/INormalizedData';
import { IUser } from '../../../../seb-model/src/interface/IUser';

export const SetUser = 'user/set-user';

export const setUser = (user : INormalizedData<IUser>) => {
    return {
        type : SetUser,
        payload : { user }
    };
};
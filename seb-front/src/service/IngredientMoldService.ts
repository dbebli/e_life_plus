import Service from './Service';

const baseUrl = 'http://localhost:3000/ingredientMold';

const IngredientMoldService = new Service(baseUrl);

export default IngredientMoldService;